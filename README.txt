OpenLayers BBox

This module provides an OpenLayers layer type plugin that allows you to add a
geojson layer to your maps that uses the OpenLayers BBox Strategy. This restricts
the features that are retrieved from the feature server to just those that lie
within the current bounding box. There is also a behavior plugin that allows you
to have the strategy only activated once a certain zoom level is reached, to
prevent large numbers of features being loaded and rendered at higher zoom levels.

Instructions
-----------------
1. Install the module and its dependencies as normal.
2. Create an OpenLayers map with a layer of type "BBox GeoJSON", entering the
path for the feature server that will deliver the features in geojson format.
3. If you want to only have the stratgey activated when a certain zoom level is
reached (meaning no features will get loaded to the layer until that zoom level
is reached), then go to the behaviors tab and check "Zoom levels for bbox strategy
layers", selecting your layer and the zoom levels at which it should be active.