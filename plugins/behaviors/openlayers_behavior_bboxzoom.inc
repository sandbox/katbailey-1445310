<?php
/**
 * @file
 * Implementation of OpenLayers behavior.
 */
$plugin = array(
  'title' => t('Zoom levels for bbox strategy layers'),
  'description' => t('Enables activation of bbox strategy layer based on zoom level.'),
  'type' => 'layer',
  'path' => drupal_get_path('module', 'openlayers_bbox') . '/plugins/behaviors',
  'file' => 'openlayers_behavior_bboxzoom.inc',
  'behavior' => array(
    'class' => 'openlayers_behavior_bboxzoom',
    'parent' => 'openlayers_behavior',
  ),
);

/**
 * Attribution Behavior
 */
class openlayers_behavior_bboxzoom extends openlayers_behavior {

  function options_form($defaults) {
    $options = array();

    foreach ($this->map['layers'] as $layer_name) {
      if ($layer_name != $this->map['default_layer']) {
        $layer = openlayers_layer_load($layer_name);
        if ($layer->data['layer_type'] == 'openlayers_layer_type_geojson_bbox') {
          $options[$layer_name] = array(
            'enabled' => array(
              '#type' => 'checkbox',
              '#title' => t('Enable zoom-level-based activation'),
              '#default_value' => isset($this->options[$layer_name]['enabled']) ? $this->options[$layer_name]['enabled'] : 0,
            ),
            'resolutions' => array(
              '#type' => 'select',
              '#multiple' => TRUE,
              '#options' => array_combine(
                array_map('strval', openlayers_get_resolutions('900913')),
                range(0, 18)
              ),
              '#title' => t('Zoom Level Range for @layer', array('@layer' => $layer_name)),
              '#default_value' => isset($this->options[$layer_name]['resolutions']) ?
                $this->options[$layer_name]['resolutions'] :
                array_map('strval', openlayers_get_resolutions('900913')),
              '#description' => t('The zoom levels at which the bbox strategy on this layer will be active.'),
            ),
          );
        }
      }
    }
    return $options;
  }

  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'openlayers_bbox') . 
      '/plugins/behaviors/openlayers_behavior_bboxzoom.js');
    return $this->options;
  }
}
