/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * OpenLayers BBox Zoom Behavior
 */
Drupal.openlayers.addBehavior('openlayers_behavior_bboxzoom', function (data, options) {
  var map = data.openlayers;
  var objContext = new Drupal.openlayers_bbox.objContext(data, options);
  for (var layerName in options) {
    if (options[layerName].enabled) {
      var layers = map.getLayersBy('drupalID', layerName);

      for (var i in layers) {
        layer = layers[i];
        for (var j in layer.strategies) {
          if (layer.strategies[j].CLASS_NAME == "OpenLayers.Strategy.BBOX") {
            map.events.on({"zoomend" : Drupal.openlayers_bbox.bboxzoom, scope: objContext });
          }
        }
      }
    }
  }
});

Drupal.openlayers_bbox = Drupal.openlayers_bbox || {};

// This is simply a means of passing the behavior scope to the function that
// we register with the "zoomend" event.
Drupal.openlayers_bbox.objContext = function(data, options) {
  this.data = data;
  this.options = options;
  return this;
}

// Event listener for on zoomend.
Drupal.openlayers_bbox.bboxzoom = function (obj) {

  var map = obj.object,
    el = $('#' + this.data.map.id),
    enabled_layers = this.options,
    base_layer_resolutions = map.baseLayer.resolutions;

  for (var name in enabled_layers) {
    if (enabled_layers.hasOwnProperty(name)) {
      // TODO: this seems backwards, figure out what's wrong here :-/
      var keys = Object.keys(enabled_layers[name].resolutions);
      var minZoom = base_layer_resolutions.indexOf(parseFloat(keys[0]));
      var layers = map.getLayersBy('drupalID', name);
      if (map.zoom >= minZoom) {
        layers[0].strategies[0].activate();
      }
      else {
        layers[0].strategies[0].deactivate();
      }
    }
  }
}
