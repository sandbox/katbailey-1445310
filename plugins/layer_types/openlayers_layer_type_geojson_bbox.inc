<?php
/**
 * @file
 * GeoJSON Layer Type
 */
$plugin = array(
  'title' => t('BBox GeoJSON'),
  'description' => t('GeoJSON with bbox filtering'),
  'layer_type' => array(
    'path' => drupal_get_path('module', 'rr_map_features'),
    'file' => 'openlayers_layer_type_geojson_bbox.inc',
    'class' => 'openlayers_layer_type_geojson_bbox',
    'parent' => 'openlayers_layer_type_geojson',
  ),
);

module_load_include('inc', 'openlayers', 'plugins/layer_types/openlayers_layer_type_geojson');

/**
 * OpenLayers GeoJSON Layer Type class
 */
class openlayers_layer_type_geojson_bbox extends openlayers_layer_type_geojson {

  /**
   * Provide initial values for options.
   */
  function options_init() {
    $options = parent::options_init();
    return array_merge($options, array('layer_type' => 'openlayers_layer_type_geojson_bbox', 'layer_handler' => 'gjbbox'));
  }

  /**
   * Options form which generates layers
   */
  function options_form($defaults = array()) {
    $form = parent::options_form($defaults);
    unset($form['geojson_data']);
    $form['layer_type']['#value'] = 'openlayers_layer_type_geojson_bbox';
    return $form;
  }

  /**
   * Render.
   */
  function render(&$map) {
    $this->data['url'] = !empty($this->data['url']) ? url($this->data['url']) : '';

    drupal_add_js(drupal_get_path('module', 'openlayers_bbox')
      . '/plugins/layer_types/openlayers_layer_type_geojson_bbox.js');
    return $this->options;
  }
}
