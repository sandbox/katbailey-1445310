/**
 * @file
 * Layer handler for GeoJSON BBox layers
 */

/**
 * Openlayer layer handler for GeoJSON layer with a bbox strategy
 */
(function($) {

Drupal.openlayers.layer.gjbbox = function(title, map, options) {

  // GeoJSON Projection handling
  var geojson_options = {
    'internalProjection': new OpenLayers.Projection('EPSG:' + map.projection),
    'externalProjection': new OpenLayers.Projection(options.projection)
  };

  var autoActivate = true;
  if (map.behaviors.openlayers_behavior_bboxzoom &&
      map.behaviors.openlayers_behavior_bboxzoom[options.drupalID].enabled) {
    autoActivate = false;
  }

  var layer = new OpenLayers.Layer.Vector(options.title, {
      drupalID: options.drupalID,
      strategies: [new OpenLayers.Strategy.BBOX({ 'autoActivate' : autoActivate })],
      protocol: new OpenLayers.Protocol.HTTP({
          url:  options.url,
          format: new OpenLayers.Format.GeoJSON(geojson_options),
          params: {
            projection: options.projection
          }
      }),
      projection: new OpenLayers.Projection('EPSG:' + options.projection),
      styleMap: Drupal.openlayers.getStyleMap(map, options.drupalID)
  });

  return layer;
};

})(jQuery);



